package com.example.databinding

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.example.databinding.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myname:MyName= MyName("Hoang Long")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.myname=myname
        binding.done.setOnClickListener {
            addNickName(it)
        }
    }
    private fun addNickName(view: View){
        binding.apply {
            myname?.nickname=editNickname.text.toString()
            invalidateAll()
            edit_nickname.visibility=View.GONE
            done.visibility=View.GONE
            textNickName.visibility=View.VISIBLE
        }
        val imm=getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}